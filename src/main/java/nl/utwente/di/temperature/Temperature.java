package nl.utwente.di.temperature;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class Temperature extends HttpServlet {
    private String calculateCelsiusToFahrenheit(String temp)
    {
        double tempC;
        try
        {
            tempC = Double.parseDouble(temp);
        }
        catch (NumberFormatException e)
        {
            return "Not a valid temperature!";
        }
        double tempF = 32 + 1.8 * tempC;
        return String.format("%.3f", tempF);
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature";

        String temp = request.getParameter("C");

        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in degrees Fahrenheit: " +
                calculateCelsiusToFahrenheit(temp) + "</BODY></HTML>");
    }
}
